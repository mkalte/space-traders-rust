#!/bin/bash
set -e
CMD="npx @openapitools/openapi-generator-cli"

function print_help {
    echo "$0 [-gh] [-r revision]"
    echo "  -g: Use a globally installed openapi-generator-cli. Otherwise npx is used."
    echo "  -h: Print this help"
    echo "  -r: use the given revision instead of the latest release."
}


revision_name=""

###################
##### Parse Options
###################
while getopts ":ghr:" option; do
    case $option in
        g)
            CMD="openapi-generator-cli";;
        h)
            print_help
            exit;;
        r)
            revision_name=${OPTARG};;
        *)
            echo "Invalid option supplied"
            print_help
            exit;;
    esac
done


echo "Using $CMD to generate sources"
echo "If you have installed openapi-generator-cli globally, and this command fails, consider using $0 -g"

if [[ -z "$revision_name" ]]; then
    echo "Grabbing newest release from spacetradersapi/api-docs"
    tarball_url=$(curl -s -L https://api.github.com/repos/spacetradersapi/api-docs/releases/latest | python -c "import sys,json; print(json.load(sys.stdin)['tarball_url'])")
else
    echo "Using supplied revision ($revision_name)"
    tarball_url="https://github.com/SpaceTradersAPI/api-docs/archive/refs/heads/$revision_name.tar.gz"
fi

rm -rf source/
mkdir -p source/
(
    set -e
    cd source/
    echo "Downloading $tarball_url"
    curl -L "$tarball_url" -o api-docs.tar.gz
    echo "Unpacking..."
    tar -xzf api-docs.tar.gz -C . --strip-components=1
)

echo "Generating..."

$CMD generate -i source/reference/SpaceTraders.json -g rust --skip-validate-spec --template-dir template --additional-properties=preferUnsignedInt=true --additional-properties=supportMultipleResponses=true

echo "Formatting generated code"
cargo fmt
