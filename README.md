[![Maintenance](https://img.shields.io/maintenance/yes/2023)]()
[![Crates.io](https://img.shields.io/crates/v/space-traders)](https://crates.io/crates/space-traders)
[![docs.rs](https://img.shields.io/docsrs/space-traders)](https://docs.rs/space-traders/latest/space-traders/)
[![Crates.io](https://img.shields.io/crates/l/space-traders)](#license)

# Rust SpaceTraders API Client

This is an async Rust client for the [SpaceTraders API](https://spacetraders.io/) game. This client is not official as are all client libraries for the game.

The APIs and models are generated from the game specification using the [OpenAPI Generator](https://github.com/OpenAPITools/openapi-generator). The generation template has been rewritten from scratch to provide a high quality client.

Please see the [getting started guide](https://docs.spacetraders.io/) for the game. The `demo` example in this repository shows using the crate to implement the steps shown in the ["New game" section](https://docs.spacetraders.io/quickstart/new-game).

## Other Clients

Here is a list of other SpaceTraders API clients in Rust.

- [`spacedust`](https://crates.io/crates/spacedust) uses the same tool to generate the client. However, it uses the default Rust template for the generator, which results in non-standard Rust code. It does use a version of the specification with more documentation.
- [`bevy_mod_pies_spacetraders_api`](https://crates.io/crates/bevy_mod_pies_spacetraders_api) is a client specifically for the `bevy` game engine.
- [`spacetraders`](https://crates.io/crates/spacetraders) looks to be abandoned.

## Minimum Supported Rust Version

Requires Rust 1.64.0.

This crate follows the ["Latest stable Rust" policy](https://gist.github.com/alexheretic/d1e98d8433b602e57f5d0a9637927e0c). The listed MSRV won't be changed unless needed.
However, updating the MSRV anywhere up to the latest stable at time of release
is allowed.

<br>

#### License

<sup>
Licensed under either of <a href="LICENSE-APACHE">Apache License, Version
2.0</a> or <a href="LICENSE-MIT">MIT license</a> at your option.
</sup>

<br>

<sub>
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in space-traders by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
</sub>
