//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

/// An orbital is another waypoint that orbits a parent waypoint.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct WaypointOrbital {
    /// The symbol of the orbiting waypoint.
    #[serde(rename = "symbol")]
    pub symbol: String,
}

impl WaypointOrbital {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(symbol: String) -> WaypointOrbital {
        WaypointOrbital { symbol }
    }
}
