//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct PurchaseCargoRequest {
    #[serde(rename = "symbol")]
    pub symbol: crate::models::TradeSymbol,
    /// Amounts of units to purchase.
    #[serde(rename = "units")]
    pub units: i32,
}

impl PurchaseCargoRequest {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(symbol: crate::models::TradeSymbol, units: i32) -> PurchaseCargoRequest {
        PurchaseCargoRequest { symbol, units }
    }
}
