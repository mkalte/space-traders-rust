//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct CreateChart201ResponseData {
    #[serde(rename = "chart")]
    pub chart: crate::models::Chart,
    #[serde(rename = "waypoint")]
    pub waypoint: crate::models::Waypoint,
}

impl CreateChart201ResponseData {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        chart: crate::models::Chart,
        waypoint: crate::models::Waypoint,
    ) -> CreateChart201ResponseData {
        CreateChart201ResponseData { chart, waypoint }
    }
}
