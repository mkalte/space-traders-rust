//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ExtractResourcesRequest {
    #[serde(rename = "survey", skip_serializing_if = "Option::is_none")]
    pub survey: Option<crate::models::Survey>,
}

impl Default for ExtractResourcesRequest {
    fn default() -> Self {
        Self::new()
    }
}

impl ExtractResourcesRequest {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new() -> ExtractResourcesRequest {
        ExtractResourcesRequest { survey: None }
    }
}
