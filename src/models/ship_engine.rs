//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

/// The engine determines how quickly a ship travels between waypoints.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ShipEngine {
    /// The symbol of the engine.
    #[serde(rename = "symbol")]
    pub symbol: Symbol,
    /// The name of the engine.
    #[serde(rename = "name")]
    pub name: String,
    /// The description of the engine.
    #[serde(rename = "description")]
    pub description: String,
    /// Condition is a range of 0 to 100 where 0 is completely worn out and 100 is brand new.
    #[serde(rename = "condition", skip_serializing_if = "Option::is_none")]
    pub condition: Option<u32>,
    /// The speed stat of this engine. The higher the speed, the faster a ship can travel from one point to another. Reduces the time of arrival when navigating the ship.
    #[serde(rename = "speed")]
    pub speed: u32,
    #[serde(rename = "requirements")]
    pub requirements: crate::models::ShipRequirements,
}

impl ShipEngine {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        symbol: Symbol,
        name: String,
        description: String,
        speed: u32,
        requirements: crate::models::ShipRequirements,
    ) -> ShipEngine {
        ShipEngine {
            symbol,
            name,
            description,
            condition: None,
            speed,
            requirements,
        }
    }
}

/// The symbol of the engine.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum Symbol {
    #[serde(rename = "ENGINE_IMPULSE_DRIVE_I")]
    ImpulseDriveI,
    #[serde(rename = "ENGINE_ION_DRIVE_I")]
    IonDriveI,
    #[serde(rename = "ENGINE_ION_DRIVE_II")]
    IonDriveIi,
    #[serde(rename = "ENGINE_HYPER_DRIVE_I")]
    HyperDriveI,
}

impl Default for Symbol {
    fn default() -> Symbol {
        Self::ImpulseDriveI
    }
}
