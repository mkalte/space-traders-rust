//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ShipRefine201ResponseDataProducedInner {
    /// Symbol of the good.
    #[serde(rename = "tradeSymbol")]
    pub trade_symbol: String,
    /// Amount of units of the good.
    #[serde(rename = "units")]
    pub units: i32,
}

impl ShipRefine201ResponseDataProducedInner {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(trade_symbol: String, units: i32) -> ShipRefine201ResponseDataProducedInner {
        ShipRefine201ResponseDataProducedInner {
            trade_symbol,
            units,
        }
    }
}
