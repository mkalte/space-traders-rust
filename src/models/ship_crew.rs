//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

/// The ship's crew service and maintain the ship's systems and equipment.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ShipCrew {
    /// The current number of crew members on the ship.
    #[serde(rename = "current")]
    pub current: i32,
    /// The minimum number of crew members required to maintain the ship.
    #[serde(rename = "required")]
    pub required: i32,
    /// The maximum number of crew members the ship can support.
    #[serde(rename = "capacity")]
    pub capacity: i32,
    /// The rotation of crew shifts. A stricter shift improves the ship's performance. A more relaxed shift improves the crew's morale.
    #[serde(rename = "rotation")]
    pub rotation: Rotation,
    /// A rough measure of the crew's morale. A higher morale means the crew is happier and more productive. A lower morale means the ship is more prone to accidents.
    #[serde(rename = "morale")]
    pub morale: u32,
    /// The amount of credits per crew member paid per hour. Wages are paid when a ship docks at a civilized waypoint.
    #[serde(rename = "wages")]
    pub wages: u32,
}

impl ShipCrew {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        current: i32,
        required: i32,
        capacity: i32,
        rotation: Rotation,
        morale: u32,
        wages: u32,
    ) -> ShipCrew {
        ShipCrew {
            current,
            required,
            capacity,
            rotation,
            morale,
            wages,
        }
    }
}

/// The rotation of crew shifts. A stricter shift improves the ship's performance. A more relaxed shift improves the crew's morale.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum Rotation {
    #[serde(rename = "STRICT")]
    Strict,
    #[serde(rename = "RELAXED")]
    Relaxed,
}

impl Default for Rotation {
    fn default() -> Rotation {
        Self::Strict
    }
}
