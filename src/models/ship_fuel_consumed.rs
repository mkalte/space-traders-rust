//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

/// An object that only shows up when an action has consumed fuel in the process. Shows the fuel consumption data.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ShipFuelConsumed {
    /// The amount of fuel consumed by the most recent transit or action.
    #[serde(rename = "amount")]
    pub amount: u32,
    /// The time at which the fuel was consumed.
    #[serde(rename = "timestamp")]
    pub timestamp: String,
}

impl ShipFuelConsumed {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(amount: u32, timestamp: String) -> ShipFuelConsumed {
        ShipFuelConsumed { amount, timestamp }
    }
}
