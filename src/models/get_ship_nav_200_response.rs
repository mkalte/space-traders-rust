//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct GetShipNav200Response {
    #[serde(rename = "data")]
    pub data: crate::models::ShipNav,
}

impl GetShipNav200Response {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(data: crate::models::ShipNav) -> GetShipNav200Response {
        GetShipNav200Response { data }
    }
}
