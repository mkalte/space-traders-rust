//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Register201Response {
    #[serde(rename = "data")]
    pub data: crate::models::Register201ResponseData,
}

impl Register201Response {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(data: crate::models::Register201ResponseData) -> Register201Response {
        Register201Response { data }
    }
}
