//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct GetShipyard200Response {
    #[serde(rename = "data")]
    pub data: crate::models::Shipyard,
}

impl GetShipyard200Response {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(data: crate::models::Shipyard) -> GetShipyard200Response {
        GetShipyard200Response { data }
    }
}
