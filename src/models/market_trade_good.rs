//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct MarketTradeGood {
    /// The symbol of the trade good.
    #[serde(rename = "symbol")]
    pub symbol: String,
    /// The typical volume flowing through the market for this type of good. The larger the trade volume, the more stable prices will be.
    #[serde(rename = "tradeVolume")]
    pub trade_volume: u32,
    /// A rough estimate of the total supply of this good in the marketplace.
    #[serde(rename = "supply")]
    pub supply: Supply,
    /// The price at which this good can be purchased from the market.
    #[serde(rename = "purchasePrice")]
    pub purchase_price: u32,
    /// The price at which this good can be sold to the market.
    #[serde(rename = "sellPrice")]
    pub sell_price: u32,
}

impl MarketTradeGood {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        symbol: String,
        trade_volume: u32,
        supply: Supply,
        purchase_price: u32,
        sell_price: u32,
    ) -> MarketTradeGood {
        MarketTradeGood {
            symbol,
            trade_volume,
            supply,
            purchase_price,
            sell_price,
        }
    }
}

/// A rough estimate of the total supply of this good in the marketplace.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum Supply {
    #[serde(rename = "SCARCE")]
    Scarce,
    #[serde(rename = "LIMITED")]
    Limited,
    #[serde(rename = "MODERATE")]
    Moderate,
    #[serde(rename = "ABUNDANT")]
    Abundant,
}

impl Default for Supply {
    fn default() -> Supply {
        Self::Scarce
    }
}
