//!
//!
//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use reqwest::StatusCode;

use super::{configuration, Error};
use crate::apis::ResponseContent;

/// Enum for successes of method [`accept_contract`].
#[derive(Debug, Clone)]
pub enum AcceptContractSuccess {
    /// Response for status code 200.
    Status200(crate::models::AcceptContract200Response),
}

impl AcceptContractSuccess {
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            200 => Some(match serde_json::from_str(body) {
                Ok(data) => Ok(Self::Status200(data)),
                Err(err) => Err(err),
            }),
            _ => None,
        }
    }
}

/// Enum for successes of method [`deliver_contract`].
#[derive(Debug, Clone)]
pub enum DeliverContractSuccess {
    /// Response for status code 200.
    Status200(crate::models::DeliverContract200Response),
}

impl DeliverContractSuccess {
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            200 => Some(match serde_json::from_str(body) {
                Ok(data) => Ok(Self::Status200(data)),
                Err(err) => Err(err),
            }),
            _ => None,
        }
    }
}

/// Enum for successes of method [`fulfill_contract`].
#[derive(Debug, Clone)]
pub enum FulfillContractSuccess {
    /// Response for status code 200.
    Status200(crate::models::FulfillContract200Response),
}

impl FulfillContractSuccess {
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            200 => Some(match serde_json::from_str(body) {
                Ok(data) => Ok(Self::Status200(data)),
                Err(err) => Err(err),
            }),
            _ => None,
        }
    }
}

/// Enum for successes of method [`get_contract`].
#[derive(Debug, Clone)]
pub enum GetContractSuccess {
    /// Response for status code 200.
    Status200(crate::models::GetContract200Response),
}

impl GetContractSuccess {
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            200 => Some(match serde_json::from_str(body) {
                Ok(data) => Ok(Self::Status200(data)),
                Err(err) => Err(err),
            }),
            _ => None,
        }
    }
}

/// Enum for successes of method [`get_contracts`].
#[derive(Debug, Clone)]
pub enum GetContractsSuccess {
    /// Response for status code 200.
    Status200(crate::models::GetContracts200Response),
}

impl GetContractsSuccess {
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            200 => Some(match serde_json::from_str(body) {
                Ok(data) => Ok(Self::Status200(data)),
                Err(err) => Err(err),
            }),
            _ => None,
        }
    }
}

/// Enum for known errors of method [`accept_contract`].
#[derive(Debug, Clone)]
pub enum AcceptContractError {}

impl AcceptContractError {
    #[allow(unused_variables, clippy::match_single_binding)]
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            _ => None,
        }
    }
}

/// Enum for known errors of method [`deliver_contract`].
#[derive(Debug, Clone)]
pub enum DeliverContractError {}

impl DeliverContractError {
    #[allow(unused_variables, clippy::match_single_binding)]
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            _ => None,
        }
    }
}

/// Enum for known errors of method [`fulfill_contract`].
#[derive(Debug, Clone)]
pub enum FulfillContractError {}

impl FulfillContractError {
    #[allow(unused_variables, clippy::match_single_binding)]
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            _ => None,
        }
    }
}

/// Enum for known errors of method [`get_contract`].
#[derive(Debug, Clone)]
pub enum GetContractError {}

impl GetContractError {
    #[allow(unused_variables, clippy::match_single_binding)]
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            _ => None,
        }
    }
}

/// Enum for known errors of method [`get_contracts`].
#[derive(Debug, Clone)]
pub enum GetContractsError {}

impl GetContractsError {
    #[allow(unused_variables, clippy::match_single_binding)]
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            _ => None,
        }
    }
}

/// Accept a contract by ID.   You can only accept contracts that were offered to you, were not accepted yet, and whose deadlines has not passed yet.
pub async fn accept_contract(
    configuration: &configuration::Configuration,
    contract_id: &str,
) -> Result<ResponseContent<AcceptContractSuccess>, Error<AcceptContractError>> {
    let client = &configuration.client;

    // Create the request from a path.
    // Make sure to url encode any user given text.
    let uri_str = format!(
        "{}/my/contracts/{contractId}/accept",
        configuration.base_path,
        contractId = crate::apis::urlencode(contract_id)
    );
    let mut req_builder = client.request(reqwest::Method::POST, &uri_str);

    // === Add headers to request ===

    // Set the user agent string if given.
    if let Some(user_agent) = &configuration.user_agent {
        req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
    }

    // === Add auth to request ===

    if let Some(token) = &configuration.bearer_access_token {
        req_builder = req_builder.bearer_auth(token.to_owned());
    };

    // === Request is built.

    // Execute the request.
    let req = req_builder.build()?;
    let resp = client.execute(req).await?;

    // Get the response.
    let status = resp.status();
    let response_body = resp.text().await?;

    if !status.is_client_error() && !status.is_server_error() {
        // Try to parse the OK response.
        match AcceptContractSuccess::from_body(status, &response_body) {
            Some(Ok(content)) => Ok(ResponseContent {
                status,
                response_body,
                content,
            }),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: false,
                response_body,
            }),
        }
    } else {
        // Try to parse the Err response.
        match AcceptContractError::from_body(status, &response_body) {
            Some(Ok(content)) => Err(Error::ResponseError(ResponseContent {
                status,
                response_body,
                content,
            })),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: true,
                response_body,
            }),
        }
    }
}

/// Deliver cargo to a contract.  In order to use this API, a ship must be at the delivery location (denoted in the delivery terms as `destinationSymbol` of a contract) and must have a number of units of a good required by this contract in its cargo.  Cargo that was delivered will be removed from the ship's cargo.
pub async fn deliver_contract(
    configuration: &configuration::Configuration,
    contract_id: &str,
    deliver_contract_request: Option<crate::models::DeliverContractRequest>,
) -> Result<ResponseContent<DeliverContractSuccess>, Error<DeliverContractError>> {
    let client = &configuration.client;

    // Create the request from a path.
    // Make sure to url encode any user given text.
    let uri_str = format!(
        "{}/my/contracts/{contractId}/deliver",
        configuration.base_path,
        contractId = crate::apis::urlencode(contract_id)
    );
    let mut req_builder = client.request(reqwest::Method::POST, &uri_str);

    // === Add headers to request ===

    // Set the user agent string if given.
    if let Some(user_agent) = &configuration.user_agent {
        req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
    }

    // === Add auth to request ===

    if let Some(token) = &configuration.bearer_access_token {
        req_builder = req_builder.bearer_auth(token.to_owned());
    };

    // === Add body to request ===

    req_builder = req_builder.json(&deliver_contract_request);

    // === Request is built.

    // Execute the request.
    let req = req_builder.build()?;
    let resp = client.execute(req).await?;

    // Get the response.
    let status = resp.status();
    let response_body = resp.text().await?;

    if !status.is_client_error() && !status.is_server_error() {
        // Try to parse the OK response.
        match DeliverContractSuccess::from_body(status, &response_body) {
            Some(Ok(content)) => Ok(ResponseContent {
                status,
                response_body,
                content,
            }),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: false,
                response_body,
            }),
        }
    } else {
        // Try to parse the Err response.
        match DeliverContractError::from_body(status, &response_body) {
            Some(Ok(content)) => Err(Error::ResponseError(ResponseContent {
                status,
                response_body,
                content,
            })),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: true,
                response_body,
            }),
        }
    }
}

/// Fulfill a contract. Can only be used on contracts that have all of their delivery terms fulfilled.
pub async fn fulfill_contract(
    configuration: &configuration::Configuration,
    contract_id: &str,
) -> Result<ResponseContent<FulfillContractSuccess>, Error<FulfillContractError>> {
    let client = &configuration.client;

    // Create the request from a path.
    // Make sure to url encode any user given text.
    let uri_str = format!(
        "{}/my/contracts/{contractId}/fulfill",
        configuration.base_path,
        contractId = crate::apis::urlencode(contract_id)
    );
    let mut req_builder = client.request(reqwest::Method::POST, &uri_str);

    // === Add headers to request ===

    // Set the user agent string if given.
    if let Some(user_agent) = &configuration.user_agent {
        req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
    }

    // === Add auth to request ===

    if let Some(token) = &configuration.bearer_access_token {
        req_builder = req_builder.bearer_auth(token.to_owned());
    };

    // === Request is built.

    // Execute the request.
    let req = req_builder.build()?;
    let resp = client.execute(req).await?;

    // Get the response.
    let status = resp.status();
    let response_body = resp.text().await?;

    if !status.is_client_error() && !status.is_server_error() {
        // Try to parse the OK response.
        match FulfillContractSuccess::from_body(status, &response_body) {
            Some(Ok(content)) => Ok(ResponseContent {
                status,
                response_body,
                content,
            }),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: false,
                response_body,
            }),
        }
    } else {
        // Try to parse the Err response.
        match FulfillContractError::from_body(status, &response_body) {
            Some(Ok(content)) => Err(Error::ResponseError(ResponseContent {
                status,
                response_body,
                content,
            })),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: true,
                response_body,
            }),
        }
    }
}

/// Get the details of a contract by ID.
pub async fn get_contract(
    configuration: &configuration::Configuration,
    contract_id: &str,
) -> Result<ResponseContent<GetContractSuccess>, Error<GetContractError>> {
    let client = &configuration.client;

    // Create the request from a path.
    // Make sure to url encode any user given text.
    let uri_str = format!(
        "{}/my/contracts/{contractId}",
        configuration.base_path,
        contractId = crate::apis::urlencode(contract_id)
    );
    let mut req_builder = client.request(reqwest::Method::GET, &uri_str);

    // === Add headers to request ===

    // Set the user agent string if given.
    if let Some(user_agent) = &configuration.user_agent {
        req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
    }

    // === Add auth to request ===

    if let Some(token) = &configuration.bearer_access_token {
        req_builder = req_builder.bearer_auth(token.to_owned());
    };

    // === Request is built.

    // Execute the request.
    let req = req_builder.build()?;
    let resp = client.execute(req).await?;

    // Get the response.
    let status = resp.status();
    let response_body = resp.text().await?;

    if !status.is_client_error() && !status.is_server_error() {
        // Try to parse the OK response.
        match GetContractSuccess::from_body(status, &response_body) {
            Some(Ok(content)) => Ok(ResponseContent {
                status,
                response_body,
                content,
            }),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: false,
                response_body,
            }),
        }
    } else {
        // Try to parse the Err response.
        match GetContractError::from_body(status, &response_body) {
            Some(Ok(content)) => Err(Error::ResponseError(ResponseContent {
                status,
                response_body,
                content,
            })),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: true,
                response_body,
            }),
        }
    }
}

/// Return a paginated list of all your contracts.
pub async fn get_contracts(
    configuration: &configuration::Configuration,
    page: Option<u32>,
    limit: Option<u32>,
) -> Result<ResponseContent<GetContractsSuccess>, Error<GetContractsError>> {
    let client = &configuration.client;

    // Create the request from a path.
    // Make sure to url encode any user given text.
    let uri_str = format!("{}/my/contracts", configuration.base_path);
    let mut req_builder = client.request(reqwest::Method::GET, &uri_str);

    // === Add queries to request ===

    if let Some(s) = &page {
        req_builder = req_builder.query(&[("page", &s.to_string())]);
    }
    // === Add queries to request ===

    if let Some(s) = &limit {
        req_builder = req_builder.query(&[("limit", &s.to_string())]);
    }

    // === Add headers to request ===

    // Set the user agent string if given.
    if let Some(user_agent) = &configuration.user_agent {
        req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
    }

    // === Add auth to request ===

    if let Some(token) = &configuration.bearer_access_token {
        req_builder = req_builder.bearer_auth(token.to_owned());
    };

    // === Request is built.

    // Execute the request.
    let req = req_builder.build()?;
    let resp = client.execute(req).await?;

    // Get the response.
    let status = resp.status();
    let response_body = resp.text().await?;

    if !status.is_client_error() && !status.is_server_error() {
        // Try to parse the OK response.
        match GetContractsSuccess::from_body(status, &response_body) {
            Some(Ok(content)) => Ok(ResponseContent {
                status,
                response_body,
                content,
            }),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: false,
                response_body,
            }),
        }
    } else {
        // Try to parse the Err response.
        match GetContractsError::from_body(status, &response_body) {
            Some(Ok(content)) => Err(Error::ResponseError(ResponseContent {
                status,
                response_body,
                content,
            })),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: true,
                response_body,
            }),
        }
    }
}
