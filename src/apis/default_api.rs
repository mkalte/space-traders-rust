//!
//!
//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use reqwest::StatusCode;

use super::{configuration, Error};
use crate::apis::ResponseContent;

/// Enum for successes of method [`get_status`].
#[derive(Debug, Clone)]
pub enum GetStatusSuccess {
    /// Response for status code 200.
    Status200(crate::models::GetStatus200Response),
}

impl GetStatusSuccess {
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            200 => Some(match serde_json::from_str(body) {
                Ok(data) => Ok(Self::Status200(data)),
                Err(err) => Err(err),
            }),
            _ => None,
        }
    }
}

/// Enum for successes of method [`register`].
#[derive(Debug, Clone)]
pub enum RegisterSuccess {
    /// Response for status code 201.
    Status201(crate::models::Register201Response),
}

impl RegisterSuccess {
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            201 => Some(match serde_json::from_str(body) {
                Ok(data) => Ok(Self::Status201(data)),
                Err(err) => Err(err),
            }),
            _ => None,
        }
    }
}

/// Enum for known errors of method [`get_status`].
#[derive(Debug, Clone)]
pub enum GetStatusError {}

impl GetStatusError {
    #[allow(unused_variables, clippy::match_single_binding)]
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            _ => None,
        }
    }
}

/// Enum for known errors of method [`register`].
#[derive(Debug, Clone)]
pub enum RegisterError {}

impl RegisterError {
    #[allow(unused_variables, clippy::match_single_binding)]
    fn from_body(status: StatusCode, body: &str) -> Option<serde_json::Result<Self>> {
        // Attempt to deserialize the response for the given status code.
        match status.as_u16() {
            _ => None,
        }
    }
}

/// Return the status of the game server. This also includes a few global elements, such as announcements, server reset dates and leaderboards.
pub async fn get_status(
    configuration: &configuration::Configuration,
) -> Result<ResponseContent<GetStatusSuccess>, Error<GetStatusError>> {
    let client = &configuration.client;

    // Create the request from a path.
    // Make sure to url encode any user given text.
    let uri_str = format!("{}/", configuration.base_path);
    let mut req_builder = client.request(reqwest::Method::GET, &uri_str);

    // === Add headers to request ===

    // Set the user agent string if given.
    if let Some(user_agent) = &configuration.user_agent {
        req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
    }

    // === Add auth to request ===

    if let Some(token) = &configuration.bearer_access_token {
        req_builder = req_builder.bearer_auth(token.to_owned());
    };

    // === Request is built.

    // Execute the request.
    let req = req_builder.build()?;
    let resp = client.execute(req).await?;

    // Get the response.
    let status = resp.status();
    let response_body = resp.text().await?;

    if !status.is_client_error() && !status.is_server_error() {
        // Try to parse the OK response.
        match GetStatusSuccess::from_body(status, &response_body) {
            Some(Ok(content)) => Ok(ResponseContent {
                status,
                response_body,
                content,
            }),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: false,
                response_body,
            }),
        }
    } else {
        // Try to parse the Err response.
        match GetStatusError::from_body(status, &response_body) {
            Some(Ok(content)) => Err(Error::ResponseError(ResponseContent {
                status,
                response_body,
                content,
            })),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: true,
                response_body,
            }),
        }
    }
}

/// Creates a new agent and ties it to an account.  The agent symbol must consist of a 3-14 character string, and will be used to represent your agent. This symbol will prefix the symbol of every ship you own. Agent symbols will be cast to all uppercase characters.  This new agent will be tied to a starting faction of your choice, which determines your starting location, and will be granted an authorization token, a contract with their starting faction, a command ship that can fly across space with advanced capabilities, a small probe ship that can be used for reconnaissance, and 150,000 credits.  > #### Keep your token safe and secure > > Save your token during the alpha phase. There is no way to regenerate this token without starting a new agent. In the future you will be able to generate and manage your tokens from the SpaceTraders website.  If you are new to SpaceTraders, It is recommended to register with the COSMIC faction, a faction that is well connected to the rest of the universe. After registering, you should try our interactive [quickstart guide](https://docs.spacetraders.io/quickstart/new-game) which will walk you through basic API requests in just a few minutes.
pub async fn register(
    configuration: &configuration::Configuration,
    register_request: Option<crate::models::RegisterRequest>,
) -> Result<ResponseContent<RegisterSuccess>, Error<RegisterError>> {
    let client = &configuration.client;

    // Create the request from a path.
    // Make sure to url encode any user given text.
    let uri_str = format!("{}/register", configuration.base_path);
    let mut req_builder = client.request(reqwest::Method::POST, &uri_str);

    // === Add headers to request ===

    // Set the user agent string if given.
    if let Some(user_agent) = &configuration.user_agent {
        req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
    }

    // === Add body to request ===

    req_builder = req_builder.json(&register_request);

    // === Request is built.

    // Execute the request.
    let req = req_builder.build()?;
    let resp = client.execute(req).await?;

    // Get the response.
    let status = resp.status();
    let response_body = resp.text().await?;

    if !status.is_client_error() && !status.is_server_error() {
        // Try to parse the OK response.
        match RegisterSuccess::from_body(status, &response_body) {
            Some(Ok(content)) => Ok(ResponseContent {
                status,
                response_body,
                content,
            }),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: false,
                response_body,
            }),
        }
    } else {
        // Try to parse the Err response.
        match RegisterError::from_body(status, &response_body) {
            Some(Ok(content)) => Err(Error::ResponseError(ResponseContent {
                status,
                response_body,
                content,
            })),
            Some(Err(err)) => Err(err.into()),
            None => Err(Error::UnknownResponse {
                status,
                is_error: true,
                response_body,
            }),
        }
    }
}
